/*
    Author: Paul Ruff
    Description: simple library for client registration per udp
*/

module.exports = class udpFind {
    constructor() {

        global.found = [];
        global.dgram = require("dgram");

        global.server = dgram.createSocket("udp4");
    }
    listen(onrecieve) {
        var type = "";
        global.server.on("message", function (msg, rinfo) {
            // if a client reponds to the check, add a timestamp to the list
            if (msg.toString() == "pong") {
                type = "alive";
                for (var obj in global.found) {
                    //obj = global.found[obj];
                    if (obj.address == rinfo.address) {
                        obj.type = type;
                        obj.online = +new Date();
                    }
                }
            } else {
                type = "register";
                // register client
                global.found.push({
                    address: rinfo.address,
                    port: rinfo.port,
                    type: type,
                    name: msg.toString().split(" ")[1]
                });
            }
            // callback
            if (onrecieve != undefined)
                onrecieve({ rinfo: rinfo, message: msg, type: type, found: global.found });
        });

        global.server.on("listening", function () {
            var address = server.address();
            console.log("server listening " + address.address + ":" + address.port);
        });

        server.bind(49999);
    }
    register(subnetmask, servicename) {
        if (servicename == undefined || servicename == "")
            return console.log("unvalid name");
        var message = new Buffer("register " + servicename);
        global.server.bind(49999, function () {
            global.server.setBroadcast(true);
        });
        // message will be sent to the 10.0.0.255 subnet by broadcast
        global.server.send(message, 0, message.length, 49999, subnetmask, function (err, bytes) {
        });
        global.server.on("message", function (msg, rinfo) {
            // respond to the alive-check with "pong"
            if (msg.toString() == "ping") {
                global.server.send("pong", 0, 4, 49999, rinfo.address);
                console.log("pong");
            }
        });
    }

    check(callback) {
        global.checkCallback = callback;
        // check if available every 10 seconds
        setInterval(this.checkRegisteredServices, 1000 * 10);
    }

    checkRegisteredServices() {
        for (var obj in global.found) {
            obj = global.found[obj];
            // send alive-check
            global.server.send("ping", 0, 4, obj.port, obj.address);

            // if the client doenst answers the requests for x mins (var sec) 
            // delete him from the list
            var sec = 30;
            var time = +new Date() - 1000 * sec;
            if (obj.online < time) {
                // remove him from the list
                // solution credit: https://stackoverflow.com/a/20690490
                global.found = global.found.filter(item => item !== obj);
            }
        }
        if (global.checkCallback != undefined)
            global.checkCallback();
    }

    simpleRun(onrecieve) {
        this.listen((data) => {
            if (onrecieve)
                return data;
            console.log("[" + data.type + "] Client " + data.rinfo.address + ":" + data.rinfo.port);
        });
        this.check(() => {
            setTimeout(function () {
                console.log(global.found);
            }, 5000);
        });
    }
};
