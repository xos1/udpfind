# udpFind
Simple library to detect services in a network

## Setup

Import library and create instance:
```Javascript
var udpFind = require("./udpFind");
var udp = new udpFind();
```

### Server

Just let the server listen: 
```Javascript
udp.simpleRun();
```

Create custom messages when recieving a packet:
```Javascript
udp.simpleRun(callback(data));
```
The function will pass a data object to the callback, it contains:
```JSON
rinfo: full rinfo object from socketconnection
type: register | alive
found: [{
    address: client address
    port: client port
    type: register | alive
    name: client name
}]
```